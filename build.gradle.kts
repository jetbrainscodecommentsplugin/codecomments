repositories.mavenCentral()

plugins {
    java
    id("org.jetbrains.intellij") version "0.4.16"
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

intellij {
    pluginName = "com.jmg.codecomment"
    updateSinceUntilBuild = false
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
group = "com.jmg"
version = "4.1"

dependencies {
    // https://mvnrepository.com/artifact/org.freemarker/freemarker
    implementation("org.freemarker", "freemarker", "2.3.28")
}