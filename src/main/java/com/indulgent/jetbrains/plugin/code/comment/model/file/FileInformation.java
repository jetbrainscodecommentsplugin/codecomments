package com.indulgent.jetbrains.plugin.code.comment.model.file;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * Information about file
 *
 * @author Indulgent
 * @since 05.06.2016.
 */
public interface FileInformation {
    /**
     * Get relative file path
     *
     * @return path
     */
    @NotNull
    String getPath();

    /**
     * Get relative file path
     *
     * @return path
     */
    VirtualFile asVirtualFile(Project project);
}
