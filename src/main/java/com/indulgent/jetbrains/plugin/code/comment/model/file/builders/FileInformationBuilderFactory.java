package com.indulgent.jetbrains.plugin.code.comment.model.file.builders;

/**
 * Builder of file information factory
 *
 * @author Indulgent
 * @since 06.06.2016.
 */
public final class FileInformationBuilderFactory {
    private static final FileInformationBuilderFactory INSTANCE = new FileInformationBuilderFactory();
    private final FileInformationBuilder builder = new FileInformationBuilderImpl();

    private FileInformationBuilderFactory() {
    }

    public static FileInformationBuilderFactory getInstance() {
        return INSTANCE;
    }

    /**
     * Get builder of file information
     *
     * @return builder
     */
    public FileInformationBuilder getBuilder() {
        return builder;
    }
}
