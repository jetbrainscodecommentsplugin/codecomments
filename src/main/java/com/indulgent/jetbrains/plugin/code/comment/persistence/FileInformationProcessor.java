package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import org.jdom.Element;

import java.util.HashMap;
import java.util.Map;

/**
 * Serializer/deserializer of file information
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
abstract class FileInformationProcessor {
    private static final String PATH_TAG_NAME = "path";
    private static final String PLACE_MODE_ATTRIBUTE_NAME = "mode";

    private final String mode;

    /**
     * Constructor
     *
     * @param mode type of file information
     */
    protected FileInformationProcessor(String mode) {
        this.mode = mode;
    }

    /**
     * Parse xml data to file information
     *
     * @param root xml data
     * @return file information
     */
    static FileInformation parseElement(Element root) {
        Element place = root.getChild(PATH_TAG_NAME);
        String mode = place.getAttribute(PLACE_MODE_ATTRIBUTE_NAME).getValue();
        return Factory.PROCESSORS.get(mode).parse(place);
    }

    /**
     * Format file information to xml data
     *
     * @param fileInformation file information
     * @return xml data
     */
    static Element formatToElement(FileInformation fileInformation) {
        return Factory.PROCESSORS.values().stream().filter(processor -> processor.accepted(fileInformation)).findFirst().map(processor -> processor.format(fileInformation)).orElse(null);
    }

    private String getMode() {
        return mode;
    }

    private FileInformation parse(Element place) {
        String filePath = place.getText();
        return doParse(filePath, place);
    }

    private Element format(FileInformation fileInformation) {
        if (!accepted(fileInformation)) {
            throw new IllegalArgumentException();
        }

        Element placeElement = new Element(PATH_TAG_NAME);
        placeElement.setAttribute(PLACE_MODE_ATTRIBUTE_NAME, getMode());
        placeElement.setText(fileInformation.getPath());
        return placeElement;
    }

    abstract boolean accepted(FileInformation fileInformation);

    protected abstract FileInformation doParse(String path, Element data);

    private static class Factory {
        static final Map<String, FileInformationProcessor> PROCESSORS = new HashMap<>();

        static {
            register(new GlobalFileInformationProcessor());
            register(new ProjectFileInformationProcessor());
        }

        private static void register(FileInformationProcessor processor) {
            PROCESSORS.put(processor.getMode(), processor);
        }
    }
}
