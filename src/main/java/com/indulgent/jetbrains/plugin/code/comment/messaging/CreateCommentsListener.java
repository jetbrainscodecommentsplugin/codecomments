package com.indulgent.jetbrains.plugin.code.comment.messaging;

/**
 * Listener of comments creation
 *
 * @author Indulgent
 * @since 01.02.2020.
 */
public interface CreateCommentsListener extends CommentsActionListener {
}
