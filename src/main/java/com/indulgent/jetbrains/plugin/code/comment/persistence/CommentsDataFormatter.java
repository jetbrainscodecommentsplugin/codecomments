package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.FileComments;
import org.jdom.Element;

import java.util.Collection;

/**
 * Formatter to xml of comments data
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
interface CommentsDataFormatter {
    /**
     * Build comments xml
     *
     * @param data comments
     * @return element
     */
    Element format(Collection<FileComments> data);
}
