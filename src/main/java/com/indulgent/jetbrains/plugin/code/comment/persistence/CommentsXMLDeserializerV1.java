package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.FileComments;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.impl.CodeInformationImpl;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.impl.CommentImpl;
import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.group.GroupInfo;
import com.indulgent.jetbrains.plugin.code.comment.model.group.GroupInfoServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfo;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfoServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.util.CommentHistoryUtil;
import com.intellij.openapi.project.Project;
import org.jdom.Element;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Deserializer of comments data for data source version equals 1
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
class CommentsXMLDeserializerV1 implements CommentsDataBuilder {
    static final String VERSION_ATTRIBUTE_NAME = "version";
    static final int VERSION = 1;

    private static final String ROOT_TAG_NAME = "root";
    private static final String COMMENTS_TAG_NAME = "comments";
    private static final String COMMENT_TAG_NAME = "comment";
    private static final String PLACE_TAG_NAME = "place";
    private static final String FROM_ATTRIBUTE_NAME = "from";
    private static final String TO_ATTRIBUTE_NAME = "to";
    private static final String TEXT_TAG_NAME = "text";
    private static final String DATA_TAG_NAME = "data";

    private final Project project;
    private final Element state;

    /**
     * Constructor
     *
     * @param project current project
     * @param state   root xml element
     */
    CommentsXMLDeserializerV1(Project project, Element state) {
        this.project = project;
        this.state = state;
    }

    @Override
    public Collection<FileComments> build() {
        GroupInfo groupInfo = GroupInfoServiceFactory.getService(project).getAll().iterator().next();
        UserInfo userInfo = UserInfoServiceFactory.getService(project).getCurrentUser();
        Map<String, FileComments> result = new HashMap<>();
        Element commentsElement = state.getChild(COMMENTS_TAG_NAME);

        for (Element commentElement : commentsElement.getChildren()) {
            Element placeElement = commentElement.getChild(PLACE_TAG_NAME);

            FileInformation fileInformation = FileInformationProcessor.parseElement(placeElement);

            String filePath = fileInformation.getPath();

            FileComments fileComments = result.get(filePath);
            if (fileComments == null) {
                fileComments = new FileCommentsImpl(fileInformation);
                result.put(filePath, fileComments);
            }

            CodeInformationImpl codeInformation = new CodeInformationImpl();
            codeInformation.setCodeText(commentElement.getChild(TEXT_TAG_NAME).getText());
            codeInformation.setStart(Integer.parseInt(placeElement.getAttribute(FROM_ATTRIBUTE_NAME).getValue()));
            codeInformation.setEnd(Integer.parseInt(placeElement.getAttribute(TO_ATTRIBUTE_NAME).getValue()));

            CommentImpl comment = new CommentImpl(fileComments.getFileInformation(), codeInformation, groupInfo);
            comment.getCommentHistory().add(userInfo, commentElement.getChild(DATA_TAG_NAME).getText());
            fileComments.save(comment);
        }
        return result.values();
    }

    static Element format(Collection<FileComments> data) {
        Element commentsElement = new Element(COMMENTS_TAG_NAME);
        for (FileComments fileComments : data) {
            for (Comment comment : fileComments.getComments()) {
                Element commentElement = new Element(COMMENT_TAG_NAME);
                Element placeElement = new Element(PLACE_TAG_NAME);
                placeElement.addContent(FileInformationProcessor.formatToElement(fileComments.getFileInformation()));
                placeElement.setAttribute(FROM_ATTRIBUTE_NAME, String.valueOf(comment.getCodeInformation().getStart()));
                placeElement.setAttribute(TO_ATTRIBUTE_NAME, String.valueOf(comment.getCodeInformation().getEnd()));
                commentElement.addContent(placeElement);
                Element textElement = new Element(TEXT_TAG_NAME);
                textElement.setText(comment.getCodeInformation().getCodeText());
                commentElement.addContent(textElement);
                Element dataElement = new Element(DATA_TAG_NAME);
                dataElement.setText(CommentHistoryUtil.getComment(comment));
                commentElement.addContent(dataElement);
                commentsElement.addContent(commentElement);
            }
        }
        Element root = new Element(ROOT_TAG_NAME);
        root.setAttribute(VERSION_ATTRIBUTE_NAME, String.valueOf(VERSION));
        root.addContent(commentsElement);
        return root;
    }
}
