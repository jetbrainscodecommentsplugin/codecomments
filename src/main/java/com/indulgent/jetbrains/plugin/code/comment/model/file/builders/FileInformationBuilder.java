package com.indulgent.jetbrains.plugin.code.comment.model.file.builders;

import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * Builder of file information
 *
 * @author Indulgent
 * @since 06.06.2016.
 */
public interface FileInformationBuilder {
    /**
     * Build file information
     *
     * @param project current project
     * @param file    current file
     * @return file information
     */
    @NotNull
    FileInformation build(@NotNull Project project, @NotNull VirtualFile file);
}
