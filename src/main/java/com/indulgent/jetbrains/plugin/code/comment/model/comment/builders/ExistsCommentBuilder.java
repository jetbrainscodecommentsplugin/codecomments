package com.indulgent.jetbrains.plugin.code.comment.model.comment.builders;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentBuilder;
import com.indulgent.jetbrains.plugin.code.comment.model.group.GroupInfo;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfo;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * Builder of exists comments
 *
 * @author Indulgent
 * @since 04.06.2016.
 */
class ExistsCommentBuilder implements CommentBuilder {
    private final Comment comment;

    private UserInfo userInfo;
    private String text;

    /**
     * Constructor
     *
     * @param comment current comment
     */
    ExistsCommentBuilder(@NotNull Comment comment) {
        this.comment = comment;
    }

    @Override
    public void setFile(@NotNull VirtualFile file) {
    }

    @Override
    public void setSelectionInformation(@NotNull SelectionModel selectionInformation) {
    }

    @Override
    public void setUserInformation(@NotNull UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public void setGroupInformation(@NotNull GroupInfo groupInfo) {
    }

    @Override
    public void setComment(@NotNull String text) {
        this.text = text;
    }

    @Override
    @NotNull
    public Comment build() {
        comment.getCommentHistory().add(userInfo, text);
        return comment;
    }
}
