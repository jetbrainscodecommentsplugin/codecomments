package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentService;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.FileComments;
import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.file.builders.FileInformationBuilderFactory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Service for work with comments
 *
 * @author Indulgent
 * @since 05.06.2016.
 */
class CommentServiceImpl implements CommentService {
    private final Project project;

    private final ConcurrentMap<String, FileComments> fileCommentsMap = new ConcurrentHashMap<>();

    /**
     * Constructor
     *
     * @param project current project
     */
    CommentServiceImpl(@NotNull Project project) {
        this.project = project;
    }

    @NotNull
    @Override
    public Collection<FileComments> getAll() {
        return fileCommentsMap.values();
    }

    @Override
    public FileComments getForFile(@NotNull VirtualFile file) {
        try {
            FileInformation fileInformation = FileInformationBuilderFactory.getInstance().getBuilder().build(project, file);
            return fileCommentsMap.get(getKey(fileInformation));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void save(@NotNull Comment comment) {
        String key = getKey(comment.getFileInformation());
        FileComments fileComments = fileCommentsMap.get(key);
        if (fileComments == null) {
            fileComments = new FileCommentsImpl(comment.getFileInformation());
            fileCommentsMap.put(key, fileComments);
        }
        fileComments.save(comment);
    }

    @Override
    public void remove(@NotNull Collection<Comment> comments) {
        comments.forEach(this::remove);
    }

    private void remove(@NotNull Comment comment) {
        String key = getKey(comment.getFileInformation());
        FileComments fileComments = fileCommentsMap.get(key);
        if (fileComments == null) {
            return;
        }
        fileComments.remove(comment);
        if (fileComments.isEmpty()) {
            fileCommentsMap.remove(key);
        }
    }

    @NotNull
    protected final Project getProject() {
        return project;
    }

    protected void setData(@NotNull CommentsDataBuilder dataBuilder) {
        fileCommentsMap.clear();
        dataBuilder.build().forEach(fileComments -> fileCommentsMap.put(getKey(fileComments.getFileInformation()), fileComments));
    }

    @NotNull
    private static String getKey(@NotNull FileInformation fileInformation) {
        return fileInformation.getPath();
    }
}
