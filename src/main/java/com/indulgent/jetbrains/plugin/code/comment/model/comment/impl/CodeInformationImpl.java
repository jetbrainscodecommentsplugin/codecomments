package com.indulgent.jetbrains.plugin.code.comment.model.comment.impl;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.CodeInformation;

/**
 * Information about code
 *
 * @author Indulgent
 * @since 04.06.2016.
 */
public class CodeInformationImpl implements CodeInformation {

    private String codeText;
    private int start;
    private int end;
    private int line = UNDEFINED;
    private int column = UNDEFINED;

    @Override
    public String getCodeText() {
        return codeText;
    }

    /**
     * Set text of commented code
     *
     * @param codeText text
     */
    public void setCodeText(String codeText) {
        this.codeText = codeText;
    }

    @Override
    public int getStart() {
        return start;
    }

    /**
     * Set start position in file
     *
     * @param start position
     */
    public void setStart(int start) {
        this.start = start;
    }

    @Override
    public int getEnd() {
        return end;
    }

    /**
     * Set end position in file
     *
     * @param end position
     */
    public void setEnd(int end) {
        this.end = end;
    }

    @Override
    public int getLine() {
        return line;
    }

    /**
     * Set line number of start commented code
     *
     * @param line line number
     */
    public void setLine(int line) {
        this.line = line;
    }

    @Override
    public int getColumn() {
        return column;
    }

    /**
     * Set column number of start commented code
     *
     * @param column column number
     */
    public void setColumn(int column) {
        this.column = column;
    }
}
