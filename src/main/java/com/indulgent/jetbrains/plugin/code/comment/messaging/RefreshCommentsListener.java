package com.indulgent.jetbrains.plugin.code.comment.messaging;

/**
 * Listener of comments refreshing
 *
 * @author Indulgent
 * @since 01.02.2020.
 */
public interface RefreshCommentsListener extends CommentsActionListener {
}
