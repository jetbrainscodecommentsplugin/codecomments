package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.file.GlobalFileInformation;
import org.jdom.Element;

/**
 * Serializer/deserializer of not-project file information
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
class GlobalFileInformationProcessor extends FileInformationProcessor {
    /**
     * Constructor
     */
    GlobalFileInformationProcessor() {
        super("global");
    }

    @Override
    public boolean accepted(FileInformation fileInformation) {
        return fileInformation instanceof GlobalFileInformation;
    }

    @Override
    protected FileInformation doParse(String path, Element data) {
        return new GlobalFileInformation(path);
    }
}
