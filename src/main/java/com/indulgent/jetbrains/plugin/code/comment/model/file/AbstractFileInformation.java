package com.indulgent.jetbrains.plugin.code.comment.model.file;

import org.jetbrains.annotations.NotNull;

/**
 * Base information about file
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
abstract class AbstractFileInformation implements FileInformation {
    private final String path;

    /**
     * Constructor
     *
     * @param path file path
     */
    protected AbstractFileInformation(String path) {
        this.path = path;
    }

    @NotNull
    @Override
    public String getPath() {
        return path;
    }
}
