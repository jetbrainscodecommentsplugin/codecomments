package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.logging.Log;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import org.jdom.Attribute;
import org.jdom.DataConversionException;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * Service for work with comments
 * Save state in project settings file
 *
 * @author Indulgent
 * @since 07.06.2016.
 */
@State(
        name = "CodeComments", storages = {
        @Storage("code-comments.xml")
})
class PersistentXMLCommentServiceImpl extends CommentServiceImpl implements PersistentStateComponent<Element> {
    private static final Map<Integer, BiFunction<Project, Element, CommentsDataBuilder>> BUILDERS = new HashMap<>();

    private static final CommentsDataFormatter CURRENT_FORMATTER = CommentsXMLDeserializerV1::format;

    static {
        BUILDERS.put(CommentsXMLDeserializerV0.VERSION, CommentsXMLDeserializerV0::new);
        BUILDERS.put(CommentsXMLDeserializerV1.VERSION, CommentsXMLDeserializerV1::new);
    }

    private final BiFunction<Project, Element, CommentsDataBuilder> UNKNOWN_VERSION_BUILDER = (project, state) -> {
        Log.getInstance(getProject()).error("Unknown comments version.");
        return Collections::emptyList;
    };

    /**
     * Constructor
     *
     * @param project current project
     */
    PersistentXMLCommentServiceImpl(@NotNull Project project) {
        super(project);
    }

    @Nullable
    @Override
    public Element getState() {
        return CURRENT_FORMATTER.format(getAll());
    }

    @Override
    public void loadState(@NotNull Element state) {
        CommentsDataBuilder deserializer = getDeserializer(state);
        setData(deserializer);
    }

    @NotNull
    private CommentsDataBuilder getDeserializer(Element state) {
        int version = parseVersion(state);
        return BUILDERS.getOrDefault(version, UNKNOWN_VERSION_BUILDER).apply(getProject(), state);
    }

    private int parseVersion(Element state) {
        Attribute versionAttribute = state.getAttribute(CommentsXMLDeserializerV1.VERSION_ATTRIBUTE_NAME);
        if (versionAttribute == null) {
            return 0;
        }

        try {
            return versionAttribute.getIntValue();
        } catch (DataConversionException e) {
            Log.getInstance(getProject()).error("Comments version " + versionAttribute.getValue() + " parsing error.", e);
            return -1;
        }
    }
}
