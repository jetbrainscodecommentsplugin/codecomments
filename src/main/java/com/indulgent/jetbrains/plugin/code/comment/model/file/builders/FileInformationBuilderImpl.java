package com.indulgent.jetbrains.plugin.code.comment.model.file.builders;

import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.file.GlobalFileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.file.ProjectFileInformation;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

/**
 * Builder of file information
 *
 * @author Indulgent
 * @since 04.06.2016.
 */
class FileInformationBuilderImpl implements FileInformationBuilder {
    /**
     * Build file information
     *
     * @param project current project
     * @param file    current file
     * @return file information
     */
    @NotNull
    public FileInformation build(@NotNull Project project, @NotNull VirtualFile file) {
        String projectPath = project.getBasePath();
        String canonicalPath = file.getCanonicalPath();
        if (canonicalPath == null) {
            throw new IllegalArgumentException("File " + file.getName() + " not found.");
        }
        if (projectPath == null || !canonicalPath.startsWith(projectPath)) {
            return new GlobalFileInformation(canonicalPath);
        }
        return new ProjectFileInformation(canonicalPath.substring(projectPath.length() + 1));
    }
}
