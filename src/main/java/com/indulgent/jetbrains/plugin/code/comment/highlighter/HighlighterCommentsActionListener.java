package com.indulgent.jetbrains.plugin.code.comment.highlighter;

import com.indulgent.jetbrains.plugin.code.comment.messaging.CreateCommentsListener;
import com.indulgent.jetbrains.plugin.code.comment.messaging.EditCommentsListener;
import com.indulgent.jetbrains.plugin.code.comment.messaging.RefreshCommentsListener;
import com.indulgent.jetbrains.plugin.code.comment.messaging.RemoveCommentsListener;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.intellij.codeInsight.daemon.DaemonCodeAnalyzer;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiManager;

import java.util.Collection;
import java.util.Objects;

/**
 * Listener of comments creation for update highlighter data
 */
public class HighlighterCommentsActionListener implements CreateCommentsListener, EditCommentsListener, RemoveCommentsListener, RefreshCommentsListener {
    private final Project project;

    /**
     * Constructor
     *
     * @param project current project
     */
    public HighlighterCommentsActionListener(Project project) {
        this.project = project;
    }

    @Override
    public void afterAction(Collection<Comment> comments) {
        PsiManager psiManager = PsiManager.getInstance(project);
        DaemonCodeAnalyzer codeAnalyzer = DaemonCodeAnalyzer.getInstance(project);
        comments.stream()
                .map(Comment::getFileInformation)
                .map(this::asVirtualFile)
                .filter(Objects::nonNull)
                .map(psiManager::findFile)
                .filter(Objects::nonNull)
                .forEach(codeAnalyzer::restart);
    }

    private VirtualFile asVirtualFile(FileInformation fileInformation) {
        return fileInformation.asVirtualFile(project);
    }
}
