package com.indulgent.jetbrains.plugin.code.comment.highlighter;

import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.FileComments;
import com.indulgent.jetbrains.plugin.code.comment.util.CommentHistoryUtil;
import com.intellij.codeInspection.*;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Optional;

/**
 * Inspection for detect and show comments in file
 *
 * @author Indulgent
 * @since 13.05.2016.
 */
public class HighlightCommentsInspection extends LocalInspectionTool {
    @Override
    public final ProblemDescriptor[] checkFile(@NotNull PsiFile file, @NotNull InspectionManager manager, boolean isOnTheFly) {
        return Optional.ofNullable(CommentServiceFactory.getService(file.getProject()).getForFile(file.getContainingFile().getVirtualFile()))
                .filter(HighlightCommentsInspection::notEmptyComments)
                .map(FileComments::getComments)
                .orElse(Collections.emptyList())
                .stream()
                .map(comment -> {
                    TextRange range = new TextRange(comment.getCodeInformation().getStart(), comment.getCodeInformation().getEnd());
                    String message = getHighlighterMessage(comment);
                    return manager.createProblemDescriptor(file, range, message, ProblemHighlightType.GENERIC_ERROR_OR_WARNING, isOnTheFly);
                })
                .toArray(ProblemDescriptor[]::new);
    }

    @NotNull
    @Override
    public PsiElementVisitor buildVisitor(@NotNull ProblemsHolder holder, boolean isOnTheFly) {
        return super.buildVisitor(holder, isOnTheFly);
    }

    private static boolean notEmptyComments(FileComments comments) {
        return !comments.isEmpty();
    }

    @NotNull
    private static String getHighlighterMessage(Comment comment) {
        return CommentHistoryUtil.getComment(comment);
    }
}
