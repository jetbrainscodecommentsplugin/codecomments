package com.indulgent.jetbrains.plugin.code.comment.model.comment;

import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfo;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * History of comments
 *
 * @author Indulgent
 * @since 05.06.2016.
 */
public interface CommentHistory {
    /**
     * Add comment
     *
     * @param userInfo author of comment
     * @param text     comment text
     */
    void add(@NotNull UserInfo userInfo, @NotNull String text);

    /**
     * Get comments
     *
     * @return comments
     */
    @NotNull
    List<CommentHistoryRecord> getRecords();
}
