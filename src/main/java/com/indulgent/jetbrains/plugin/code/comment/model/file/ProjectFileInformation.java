package com.indulgent.jetbrains.plugin.code.comment.model.file;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;

/**
 * Information about file of project
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
public class ProjectFileInformation extends AbstractFileInformation {
    /**
     * Constructor
     *
     * @param path file path
     */
    public ProjectFileInformation(String path) {
        super(path);
    }

    @Override
    public VirtualFile asVirtualFile(Project project) {
        return VirtualFileManager.getInstance().findFileByUrl("file://" + project.getBasePath() + "/" + getPath());
    }
}
