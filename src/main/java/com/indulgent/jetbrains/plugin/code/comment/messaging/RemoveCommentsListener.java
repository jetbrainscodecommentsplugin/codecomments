package com.indulgent.jetbrains.plugin.code.comment.messaging;

/**
 * Listener of comments removing
 *
 * @author Indulgent
 * @since 01.02.2020.
 */
public interface RemoveCommentsListener extends CommentsActionListener {
}
