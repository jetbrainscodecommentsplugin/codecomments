package com.indulgent.jetbrains.plugin.code.comment.persistence;

import com.indulgent.jetbrains.plugin.code.comment.model.file.FileInformation;
import com.indulgent.jetbrains.plugin.code.comment.model.file.ProjectFileInformation;
import org.jdom.Element;

/**
 * Serializer/deserializer of project file information
 *
 * @author Indulgent
 * @since 21.03.2020.
 */
class ProjectFileInformationProcessor extends FileInformationProcessor {
    /**
     * Constructor
     */
    ProjectFileInformationProcessor() {
        super("project");
    }

    @Override
    public boolean accepted(FileInformation fileInformation) {
        return fileInformation instanceof ProjectFileInformation;
    }

    @Override
    protected FileInformation doParse(String path, Element data) {
        return new ProjectFileInformation(path);
    }
}
