package com.indulgent.jetbrains.plugin.code.comment.view.actions;

import com.indulgent.jetbrains.plugin.code.comment.messaging.MessagingProvider;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.Comment;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentBuilder;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentHistoryRecord;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.CommentService;
import com.indulgent.jetbrains.plugin.code.comment.model.comment.builders.CommentBuilderFactory;
import com.indulgent.jetbrains.plugin.code.comment.model.user.UserInfoServiceFactory;
import com.indulgent.jetbrains.plugin.code.comment.view.component.CommentToolWindowFactory;
import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Action for edit selected comment
 *
 * @author Indulgent
 * @since 15.05.2016.
 */
public class EditCommentAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getProject();
        if (project == null) {
            Messages.showInfoMessage("File not found.", "Attention");
            return;
        }

        Collection<Comment> comments = CommentToolWindowFactory.getToolWindowComments(project);
        if (comments.isEmpty()) {
            Messages.showInfoMessage("Select comment.", "Attention");
            return;
        }
        if (comments.size() > 1) {
            Messages.showInfoMessage("Select single comment.", "Attention");
            return;
        }

        Comment selectedComment = comments.iterator().next();
        String codeText = selectedComment.getCodeInformation().getCodeText();
        List<CommentHistoryRecord> records = selectedComment.getCommentHistory().getRecords();
        String commentText = Messages.showMultilineInputDialog(project, "Comment to:\n" + codeText, "Comment", records.get(0).getText(), AllIcons.General.Balloon, null);
        if (commentText == null || commentText.length() == 0) {
            return;
        }

        CommentBuilder builder = CommentBuilderFactory.getInstance().getBuilder(selectedComment);
        builder.setComment(commentText);
        builder.setUserInformation(UserInfoServiceFactory.getService(project).getCurrentUser());
        CommentService service = ServiceManager.getService(project, CommentService.class);
        Comment comment = builder.build();
        service.save(comment);

        MessagingProvider.getInstance(project).sendEditComments(Collections.singletonList(comment));
    }
}
