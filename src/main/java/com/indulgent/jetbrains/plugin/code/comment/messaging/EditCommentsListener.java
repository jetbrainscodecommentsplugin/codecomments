package com.indulgent.jetbrains.plugin.code.comment.messaging;

/**
 * Listener of comments editing
 *
 * @author Indulgent
 * @since 01.02.2020.
 */
public interface EditCommentsListener extends CommentsActionListener {
}
